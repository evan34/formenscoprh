//* quand la page est chargée et au click sur le bouton connect
$().ready(function () {




    $('#button_update_training').click(function () {
        //* recupère les données du formulaire dans une variable
        var form_data = $('#update_training_form').serialize();
        var id = $('#training_id').val();
        //alert(id);
        //alert(form_data);

        //* requête ajax en POST vers la page qui gère la connexion + data_form
        $.ajax({
            url: "/training/" + id,
            method: "PUT",
            data: form_data,
            //* si la requête réussi 
            success: function (result) {
                //alert(result);
                if (result === 'true') {
                    $('#response').html(
                        "<div class='alert alert-success'>Formation mise à jour avec succès !</div>")
                    $('#button_update_training').html(
                        "Enregistrement <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")


                    setTimeout(
                        function () {
                            window.location.href = "/trainings";

                        }, 2000);

                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>Role : Il y a un probleme !</div>")
                }
            },

            error: function () {
                window.location.replace("/404");
            }

        })
    })

});