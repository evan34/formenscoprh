//* quand la page est chargée et au click sur le bouton connect
$().ready(function () {

    $('#button_update_account').click(function () {
        //* recupère les données du formulaire dans une variable
        var form_data = $('#update_account_form').serialize();
        var id = $('#account_id').val();
        //alert(id);


        //* requête ajax en POST vers la page qui gère la connexion + data_form
        $.ajax({
            url: "/account/" + id,
            method: "PUT",
            data: form_data,
            //* si la requête réussi 
            success: function (result) {




                if (result === 'true') {
                    $('#response').html(
                        "<div class='alert alert-success'>Compte mise à jour avec succès !</div>")
                    $('#button_update_account').html(
                        "Enregistrement <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")


                    setTimeout(
                        function () {
                            document.location.reload(true)
                        }, 2000);

                } else {
                    //alert(result)
                    $('#response').html(
                        "<div class='alert alert-danger'>Edit Account : Il y a un probleme!</div>")
                }
            },

            error: function () {
                window.location.replace("/404");
            }

        })
    })

});