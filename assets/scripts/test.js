console.log('test');

function eventClic(idEvent, accountEventid, state, idguest) {

    //* ouvre la modale
    $('.newEvent').attr("data-toggle", "modal");
    $('.newEvent').attr("data-target", "#modal-update");
    let userId = $('.idHide').text();
    let id = idEvent;
    console.log('guest: '+idguest);

    //! pré-rempli les champs de la modal avec les differentes valeurs de l'event
    //* requête ajax pour récuperer toutes les infos d'un event
    $.ajax({
        url: "http://formenscop.bwb/calendar",
        method: "POST",
        dataType: "JSON",
        data: {
            id: id
        },
        //* si la requête se passe bien!!
        success: function (result) {
            //* pré-rempli les champs de la modale avec les valeurs stockées en bdd reçues en result de la requête
            $('#nameEvent').attr("value", result[0].title);
            $('#descriptionEvent').text(result[0].description);
            $('#startTimeEvent').attr('value', result[0].start)
            $('#endTimeEvent').attr("value", result[0].end);
            //* modifie le titre de la modale en fonction du type d'event et du creator
            $('#EventTilteUpdate').text("Type d'event: " + result[0].type + " - Crée par: " + result[0].firstName)
            //* requête ajax pour récuperer toutes les invités d'un event
            let id = idEvent;
            $.ajax({
                url: `http://formenscop.bwb/calendar/guests`,
                method: "POST",
                data: {
                    id: id
                },
                success: function (res) {
                    let guests = JSON.parse(res);
                    console.log(guests);
                    //* vide la div ou sont affichés les nomns des invités
                    $('.displayGuestupdate').empty();
                    //* parcourir le tableau pour afficher les élements au bon endroit !!
                    $('.displayGuestupdate').append('<h5>Invités</h5>')
                    for (let i = 0; i < guests.length; i++) {
                        let idguest = guests[i].Account_id_guest;
                        let state = guests[i].state;
                        let icone;
                        //* icone placée a côté du nom de l'invité en fonction de sa réponse a l'event
                        if (state === 'no') {
                            icone = '<i class="fas fa-times"></i>';
                        } else if (state === 'maybe') {
                            icone = '<i class="fas fa-question"></i>'
                        } else {
                            icone = '<i class="fas fa-check"></i>';
                        }
                        if (idguest !== userId) {
                            console.log('guest');
                            $('.displayGuestupdate').append("<div id=update" + idguest + " class='divguest'>" + guests[i].FirstName + "  " + icone + " </div>");
                        } else {
                            console.log('ajax');
                            $.ajax({
                                url: "http://formenscop.bwb/calendar/state",
                                method: "POST",
                                data: {
                                    'state': 'yes+',
                                    'idAccountEvent': accountEventid
                                },
                                success: function (result) {
                                    alert(result);
                                }
                            })
                        }
                    }
                    // //! La modale est remplie avec les bonnes infos !!
                    // //* si le créateur de l'event n'est pas l'utilisateur connécté
                  // alert('guest: '+guest+'-user: '+userId);
                    if (parseInt(idguest) == parseInt(userId)) {
                        console.log('guest');
                        //* grise les champs et cache les bouton d'update et delete
                        $('#nameEvent').prop('disabled', true);
                        $('#descriptionEvent').prop('disabled', true);
                        $('#startTimeEvent').prop('disabled', true);
                        $('#endTimeEvent').prop('disabled', true);
                        $('#updateButton').hide();
                        $('#deleteButton').hide()
                        //* si l'event a été suprimé
                        if (state == "nosuppr") {
                                    $('#EventTilteUpdate').html("EVENT ANNULÉ");
                                    $('#EventTilteUpdate').attr('class', 'alert alert-danger');
                                    $('#validateButton').hide();
                                    $('#refuseButton').hide();
                                    $('#eventannul').show();
                                    $.ajax({
                                        url: "http://formenscop.bwb/calendar/state",
                                        method: "POST",
                                        data: {
                                            'state': 'del',
                                            'idAccountEvent': accountEventid
                                        },
                                        success: function (result) {
                                            alert(result);
                                        }
                                    })
                            //* si l'event est en attente de validation
                                }  else if(state === 'maybe'){
                                    console.log('maybe');
                                    $('#validateButton').show();
                                    $('#refuseButton').show();
                                    $('#validateButton').click(function () {
                                        // //! AU CLIC SUR LE BOUTON VALIDER
                                        $.ajax({
                                            url: "http://formenscop.bwb/calendar/state",
                                            method: "POST",
                                            data: {
                                                'state': 'yes',
                                                'idAccountEvent': accountEventid
                                            },
                                            success: function (result) {
                                                alert(result);
                                            }
                                        })
                                    })
                                    // //! AU CLIC SUR LE BOUTON REFUSER
                                    $('#refuseButton').click(function () {
                                       // alert(accountEventid);
                                        $.ajax({
                                            url: "http://formenscop.bwb/calendar/state",
                                            method: "POST",
                                            data: {
                                                'state': 'no',
                                                'idAccountEvent': accountEventid
                                            },
                                            success: function (result) {
                                                console.log('no');
                                                alert(result);
                                            }
                                        })
                                        //todo requête ajax vers /refuser
                                        //todo si success alert(success)
                                    })
                                    
                                }else{
                                    $('#validateButton').hide();
                                    $('#refuseButton').hide();
                                }
                    //* Si l'utilisateur de l'event est le créateur de l'event
                    } else {
                        console.log('creator');
                        $('#validateButton').hide();
                        $('#refuseButton').hide();
                        $('#updateButton').hide();
                        $('#deleteButton').hide();
                        if (state == "yes") {
                            console.log('yes');
                          //todo div info
                          $.ajax({
                              url: "http://formenscop.bwb/calendar/state",
                              method: "POST",
                              data: {
                                  'state': 'validate',
                                  'idAccountEvent': accountEventid
                                },
                                success: function (result) {
                                    alert(result);
                                    $('#EventTilteUpdate').text("un invité a validé sa présence a l'event");
                                    $('#EventTilteUpdate').attr('class', 'alert alert-success');
                                    console.log('validate');
                                }
                            })
                        } else if (state == "no") {
                            console.log('no');
                           //todo div info
                           $.ajax({
                               url: "http://formenscop.bwb/calendar/state",
                               method: "POST",
                               data: {
                                   'state': 'del',
                                   'idAccountEvent': accountEventid
                                },
                                success: function (result) {
                                    alert(result);
                                    $('#EventTilteUpdate').text("un invité a refusé votre invitation a l'event");
                                   $('#EventTilteUpdate').attr('class', 'alert alert-danger');
                                   console.log('refusé')
                                }
                            })
                        }
                    }

                }
            })

        }

    })
}

