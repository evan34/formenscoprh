<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<!-- ICI IL FAUT QUE J ARRIVE A VIRER LE BACK GROUND DU BOUTON-->
							<a href="#" class="btn-block" data-toggle="modal" data-target="#bd-example-modal-lg-test" type="hidden">

							</a>
							<div class="modal fade bs-example-modal-lg" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="EventTilteUpdate"></h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">

											<form method="PUT">
												<div class="row">
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Début de l'evenement</label>
															<input id="startTimeEvent" type="date" class="datetimepicker form-control" name="start">

														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Fin de l'évenement</label>
															<input id="endTimeEvent" type="date" class="datetimepicker form-control" name="end">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Nom de l'evenement</label>
															<input id="nameEvent" type="text" class="form-control" name="title">
														</div>
													</div>
													
												</div>
												<!-- <div class="col-md-4 col-sm-12"> -->
														<div class="row col-sm-12">
															
																	<div class="displayGuestupdate ml-4 ">
		
																	</div>
																	
															<input type="text" class="d-none" id='guestsupdate' name='guest'>
														</div>
													<!-- </div> -->
												<textarea id="descriptionEvent" rows="5" cols="40">

												</textarea>

												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermeture</button>
													<button id="updateButton" class="btn btn-primary">Modifier</button>
													<button id="deleteButton" class="btn btn-danger">Supprimer</button>
													<button id="validateButton" class="btn btn-success">Valider l'évenement</button>
													<button id="refuseButton" class="btn btn-danger">Refuser l'évenement</button>
													<div id="eventannul" class="d-none alert alert-danger">EVÈNEMENT ANNULÉ!!</div>
												</div>

										</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
