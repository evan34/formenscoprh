<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
	<link rel="stylesheet" type="text/css" href="assets/plugins/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/datatables/media/css/responsive.dataTables.css">
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>List of modules</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">modules</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Simple Datatable start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

					<div class="row">
						<table class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th>id</th>
									<th>Module</th>
									<th>Description</th>
									<th>Start</th>
									<th>Volume</th>

									<th class="datatable-nosort">Action</th>

								</tr>
							</thead>
							<tbody>

								<?php foreach ($datas[0] as $data) : ?>
									<tr>
										<td class="table-plus"><?= $data->getId(); ?></td>
										<td><?= $data->getName(); ?></td>
										<td><?= $data->getDesciption(); ?></td>
										<td><?= $data->getStart(); ?></td>
										<td><?= $data->getVolume() . " jours"; ?></td>


										<td>
											<div class="dropdown">
												<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
													<i class="fa fa-ellipsis-h"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="/details-module?edit=<?= $data->getId(); ?>"><i class="fa fa-eye"></i> View</a>
													<a class="dropdown-item" href="/edit-module?edit=<?= $data->getId(); ?>" name="edit"><i class="fa fa-pencil"></i> Edit</a>




												</div>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</div>
				<!-- Simple Datatable End -->
			</div>

			<?php
			// if(isset($_GET['edit'])) {
			// 	$id = $_GET['id'];
			// }

			?>

			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
	<script src="assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="assets/plugins/datatables/media/js/dataTables.responsive.js"></script>
	<script src="assets/plugins/datatables/media/js/responsive.bootstrap4.js"></script>

	<script>
		$('document').ready(function() {
			$('.data-table').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Search"
				},
			});

		});
	</script>
</body>

</html>