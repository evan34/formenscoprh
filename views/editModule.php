<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Edit module</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Edit module</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- Default Basic Forms Start -->
					<!-- Default Basic Forms Start -->
					<!-- Default Basic Forms Start -->

					<form id="update_module_form">
						<h3>Module</h3><br><br>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Name </label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" type="text" placeholder="" name="name" value="<?= $datas[0]->getName(); ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Description</label>
							<div class="form-group col-md-10">
								<input class="form-control" placeholder="description module" type="text" name="desciption" value="<?= $datas[0]->getDesciption(); ?>">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Volume</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" placeholder="volume module" type="days" name="volume" value="<?= $datas[0]->getVolume(); ?>">
							</div>
						</div>
						<!-- modifications formation-->
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Formation</label>
							<div class="col-sm-12 col-md-10">
								<select class="custom-select col-12" name="Training_id">
									<option value="<?= $datas[0]->getTraining_id(); ?>"><?= ($datas[1][$datas[0]->getTraining_id() - 1]->getName()); ?></option>

									<?php foreach ($datas[1] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName(); ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div id="response"></div>

						<br>

						<input id="module_id" type="hidden" name="id" value="<?= $datas[0]->getId(); ?>">
						<button id="button_update_module" type="button" class="btn btn-primary">Editer</button>

					</form><br>

					<form id="delete_module_form">
						<input type="hidden" id="module_id" name="id" value="<?= $datas[0]->getId(); ?>">

						<button id="button_delete_module" type="button" class="btn btn-danger" name="delete">Supprimer</button>
					</form>
				</div>
				<!-- Default Basic Forms End -->
			</div>

			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>