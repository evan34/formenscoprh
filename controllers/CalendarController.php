<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOEventType;
use BWB\Framework\mvc\dao\DAOEvent;
use BWB\Framework\mvc\dao\DAOAccountEvent;
use BWB\Framework\mvc\dao\DAOEventState;
use BWB\Framework\mvc\dao\DAOAccountCalendar;
use BWB\Framework\mvc\dao\DAOCalendar;
use function GuzzleHttp\json_encode;

class CalendarController extends Controller
{

    /**
     * 
     * This Method is bringing back all the data's Event from the database
     * 
     */
    public function getAll()
    {
        //! $guests a faire en fonction de l'annuaire au moment de la methode render sur chaque api !!!
        $guests = (new DAOAccountCalendar())->getAll();
        $array = array('guests' => $guests);
        $this->render("calendar", $array);
    }

    /**
     * 
     * This Method is permitting the update of the event and send it to the database
     * 
     */
    public function updateEVent()
    {
        //* on recupère l'id du type d'event grace a la methode getTypeId : retourne un tableau associatif ('id' => id)
       $eventTypeId = (new DAOEventType())->getTypeId($this->inputPut()['type'])['id'];
       //* on prepare le tableau pour effectuer l'update de la table Event
       $array = array(
           'id' => $this->inputPut()['id'],
        'start' => $this->inputPut()['start'],
        'end' => $this->inputPut()['end'],
        'name' => $this->inputPut()['title'],
        'description' => $this->inputPut()['description'],
        'type_id' => $eventTypeId
    );
        //* on appel la method update du DAOEvent si tout se passe bien on passe a la suite
        if ((new DAOEvent())->update($array) === true) {
             //* si l'event n'a pas d'invité creator = guest
             if (empty($this->inputPut()['guest'])) {
                 //* on prépare le tableau ppur effectuer l'update de la table AccountEvent
                 $array2 = array(
                    'Account_id_creator' => $this->inputPut()['creator'],
                    'Account_id_guest' => $this->inputPut()['creator'],
                    'Eventid' => $this->inputPut()['id']
                );
                     //* evenement perso sans invité : creator = guest => event state = yes+ (pas de notification)
                     if ((new DAOEventState)->update('yes+')) {
                         
                        echo 'update ok!!';
                    
                }
                //* si il y a un ou plusieurs invités
             }else{
                 //* on recupère le string guest et on l'explode dans un tableau
                $guests = $this->inputPut()['guests'];
                $arrayGuest = explode("/", $guests);
                //* pour chaque index du tableau si il n'est pas null ou vide
                foreach($arrayGuest as $key => $value){
                    if($value !== "" || $value !== "null"){
                        $eventAccountId = (new DAOAccountEvent())->getId( $this->inputPut()['id'], $value);
                        $array2 = ['maybe', $eventAccountId];
                            if(($this->inputPut()['type'] === 'rdv' && (new DAOEventState)->updateEVent($array2)) || ($this->inputPut()['type'] === 'autre' && (new DAOEventState)->update($array2))){
                                echo 'create ok!!';
                            }
                        }
                    }
                }
                
             

        }else {
            echo 'erreur';
        }
    // $guests = $this->inputPut()['guests'];
    // $arrayGuest = explode("/", $guests);
    //     echo json_encode($arrayGuest);
       
       
    }

    public function retrieve()
    {
        $calendar = new DAOCalendar();
        $id = $this->inputPost()['id'];
        //var_dump($calendar->retrieve($id));
       echo (json_encode($calendar->retrieve($id)));
    }

    public function deleteEvent()
    {
       $idEvent = $this->inputPost()['id'];
       $guests= (new DAOCalendar)->getGuests($idEvent);
        foreach($guests as $key => $guest){
        $guestid = $guest['Account_id_guest'];
        $AccountEventId = (new DAOAccountEvent())->getId($idEvent, $guestid)['id'];
      if(intval($guestid) === intval($this->inputPost()['userId'])){
        if( (new DAOEventState())->update(['no+', $AccountEventId])){
            echo 'delete1';
        }
      }else{
          if( (new DAOEventState())->update(['nosuppr', $AccountEventId])){
             echo 'delete2';
         }
      }
       }
}
      
  
    public function createEvent()
    {
        
        //* recuperer id type event
        $eventTypeId = (new DAOEventType())->getTypeId($this->inputPost()['type'])['id'];
        //* tableau a envoyé au DAO pour créer ligne table EVENT
        $array = array(
            'start' => $this->inputPost()['start'],
            'end' => $this->inputPost()['end'],
            'name' => $this->inputPost()['title'],
            'description' => $this->inputPost()['description'],
            'type_id' => $eventTypeId
        );
        //* si la création se passe bien on continue...
        if ((new DAOEvent())->create($array) === true) {
            //*on recupère l'id de l'event créer
            $idEvent = (new DAOEvent())->getLastId();
            //? si l'event n'a pas d'invité creator = guest
            if (empty($this->inputPost()['guest'])) {
                $array2 = array(
                    'Account_id_creator' => $this->inputPost()['id'],
                    'Account_id_guest' => $this->inputPost()['id'],
                    'Eventid' => $idEvent
                );
                if ((new DAOAccountEvent())->create($array2) === true) {
                    //? evenement perso sans invité : creator = guest => event state = yes+ (pas de notification)
                    if ((new DAOEventState)->create('yes+')) {
                        echo 'create ok!!';
                    }
                }
            //? si l'event a un ou plusieurs invités
            }else{
                $guests = $this->inputPost()['guest'];
                $arrayGuest = explode("/", $guests);
                foreach($arrayGuest as $key => $value){
                    if($value !== "" || $value !== "null"){
                        $array2 = array(
                            'Account_id_creator' => $this->inputPost()['id'],
                            'Account_id_guest' => $value,
                            'Eventid' => $idEvent
                        );
                        if ((new DAOAccountEvent())->create($array2) === true) {
                            if(($this->inputPost()['type'] === 'rdv' && (new DAOEventState)->create('maybe')) || ($this->inputPost()['type'] === 'autre' && (new DAOEventState)->create('maybe'))){
                                echo 'create ok!!';
                            }
                        }
                    }
                }
            }
        } else {
            echo 'erreur';
        }
    }

   //* prend en parametre l'id de l'utilisateur
    public function getModulesJson($id)
    {  
        $calendar =  new DAOCalendar();
        echo json_encode($calendar->getModules($id));
    }
    //* prend en parametre l'id de l'utilisateur
    public function getAutresJson($id)
    {  
        $calendar =  new DAOCalendar();
        echo json_encode($calendar->getAutres($id));
    }
    //* prend en parametre l'id de l'utilisateur
    public function getRdvJson($id)
    {  
        $calendar =  new DAOCalendar();
        echo json_encode($calendar->getRdv($id));
        //$calendar->getRdv($id);
    }

    
    public function getGuest()
    {
       $idEvent = $this->inputPost()['id'];
       $guests = (new DAOCalendar())->getGuests($idEvent);
       echo(json_encode($guests));
    }

    public function stateEvent()
    {
        $array = [$this->inputPost()['state'], $this->inputPost()['idAccountEvent']];
        $update = (new DAOEventState())->update($array);
        echo(json_encode($update));
    }

   
   }
