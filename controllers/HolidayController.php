<?php
namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\dao\DAOHoliday;
use BWB\Framework\mvc\Controller;

class HolidayController extends Controller
{

    /**
     * Méthode invoquée quand la demande de congés est validée
     *
     * @return void
     */
    public function validateRequest() {
        $id = $_GET;
        return (new DAOHoliday())->validateRequest($id);
        $this->render("listeHolidayRequests");
    }
    

    /**
     * Méthode invoquée quand la demande de congés est refusée
     *
     * @return void
     */
    public function refuseRequest() {
        $id = $_GET;
        return (new DAOHoliday())->refuseRequest($id);
        $this->render("listeHolidayRequests");
    }

}