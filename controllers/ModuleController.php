<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\dao\DAOModule;
use BWB\Framework\mvc\dao\DAOTraining;
use BWB\Framework\mvc\Controller;

class ModuleController extends Controller
{
    
    /**
     * Retourne le formulaire de création de module
     *
     * @return void
     */
    public function getCreateModule()
    {
        $datas = (new DAOTraining())->getAll();
        $this->render('createModule', $datas);
    }

    /**
     * Méthode qui crée un module ou le supprime si il possède un id
     *
     * @return void
     */
    public function createEndDelete()
    {
        $array = $this->inputPost();
        if (isset($array['id'])) {
            echo ((new DAOModule)->deleteModule($array)) ?  'true' : 'false';
        } else {
            echo ((new DAOModule())->create($array)) ?  'true' : 'false';
        }
    }

    /**
     * Retourne le formulaire de modification d'un module par id 
     * Méthode qui permet de récupérer et d'afficher toutes les données du module existant
     *
     * @return void
     */
    public function formEditModule()
    {
        $id = $this->inputGet();
        $dataModule = (new DAOModule())->retrieve($id);
        $dataTraining = (new DAOTraining())->getAll();
        $datas = array($dataModule, $dataTraining);
        $this->render('editModule', $datas);
    }

    /**
     * Méthode invoquée pour update un module sélectionné
     *
     * @return void
     */
    public function updateById()
    {
        $datas = $this->inputPut();
        //print_r($datas);die;

        if ((new DAOModule)->update($datas)) {
            echo 'true';
        } else {
            echo 'false';
        };
    }

    /**
     * Méthode invoquée pour delete (cacher) un module
     *
     * @return void
     */
    public function deleteModule()
    {
        $id = $_GET;
        return (new DAOModule())->deleteModule($id);
        $this->render("module");
    }

    /**
     * Méthode qui retourne la vue du détails d'un module par id
     *
     * @return void
     */
    public function getAllById()
    {
        $id = $_GET;
        $datas = (new DAOModule())->getAllbyId($id);
        $this->render("detailModule", $datas);
    }
}
