<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOType extends DAO
{
    /**
     * retrourne les types 
     *
     * @return array
     */
    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT * FROM Type');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Type');
        $res = $result->fetchAll();
        return $res;
    }

    /**
     * retourne un type en fonction de son id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $query = "SELECT * FROM Type WHERE id=" . $id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }

    public function getAllBy($array)
    { }
    public function update($array)
    { }
    public function create($array)
    { }
    public function delete($id)
    { }
}
