<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOTraining extends DAO
{

    /**
     * Crée une formation
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function create($array)
    {
        $name = $array['name'];
        $start = $array['start'];
        $end = $array['end'];
        $volume = $array['volume'];

        // On rempli la table Training
        $trainingTable = ['name' => $name, 'start' => $start, 'end' => $end, 'volume' => $volume];

        $query1 = "INSERT INTO Training (name, start, end, volume) VALUES (:name, :start, :end, :volume)";

        $resultat1 = $this->getPdo()->prepare($query1);
        $resultat1->execute($trainingTable);

        //Récupération de l'ID de l'address (foreign key)
        $training_new_id = $this->getPdo()->lastInsertId();

        // On rempli la table Trainer
        $trainer = $array['Salaried_Account_id'];
        $coordinator = $array['Coordinator_Salaried_Account_id'];

        $trainerTable = ['Salaried_Account_id' => $trainer, 'Coordinator_Salaried_Account_id' => $coordinator, 'Training_id' => $training_new_id];
        $query2 = "INSERT INTO Trainer (Salaried_Account_id, Coordinator_Salaried_Account_id, Training_id) VALUES (:Salaried_Account_id, :Coordinator_Salaried_Account_id, :Training_id)";

        $resultat2 = $this->getPdo()->prepare($query2);
        return $resultat2->execute($trainerTable);
    }

    /**
     * Retourne toutes les formations
     *
     * @return array
     */
    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT ti.id, ti.name, ti.start, ti.end, ti.volume, ac1.Name as formateur, ac2.Name as coordinateur FROM Training as ti JOIN Trainer as tr ON ti.id = tr.Training_id JOIN Account as ac1 ON ac1.id = tr.Salaried_Account_id JOIN Account as ac2 ON ac2.id = tr.Coordinator_Salaried_Account_id');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Training');
        return $result->fetchAll();
    }


    /**
     * retourne une formation grace a son id
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $query = "SELECT ti.id, ti.name, ti.start, ti.end, ti.volume, ac1.id as idformateur, ac1.Name as formateur, ac2.id as idcoordinateur, ac2.Name as coordinateur FROM Training as ti JOIN Trainer as tr ON ti.id = tr.Training_id JOIN Account as ac1 ON ac1.id = tr.Salaried_Account_id JOIN Account as ac2 ON ac2.id = tr.Coordinator_Salaried_Account_id WHERE ti.id=" . $id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Training');
        return $result->fetch();
    }


    /**
     * mise a jour d'une formation
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function update($array)
    {
        $id = $array["id"];
        $name = $array['name'];
        $start = $array['start'];
        $end = $array['end'];
        $volume = $array['volume'];

        $training = array('id' => $id, 'name' => $name, 'start' => $start, 'end' => $end, 'volume' => $volume);

        $query1 = "UPDATE Training SET name='" . $name . "', start='" . $start . "',end='" . $end . "', volume='" . $volume . "' WHERE id='" . $id . "'";

        $resultTraining = $this->getPdo()->prepare($query1);
        $resultTraining->execute($training);

        $Salaried_Account_id = $array['Salaried_Account_id'];
        $Coordinator_Salaried_Account_id = $array['Coordinator_Salaried_Account_id'];

        $trainer = array('Training_id' => $id, 'Salaried_Account_id' => $Salaried_Account_id, 'Coordinator_Salaried_Account_id' => $Coordinator_Salaried_Account_id);

        $query2 = "UPDATE Trainer SET Salaried_Account_id='" . $Salaried_Account_id . "', Coordinator_Salaried_Account_id='" . $Coordinator_Salaried_Account_id . "' WHERE Training_id='" . $id . "'";

        $resultTrainer = $this->getPdo()->prepare($query2);
        return $resultTrainer->execute($trainer);
    }
    
    public function delete($id)
    { }
    public function getAllBy($filter)
    { }
}
