<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;


class DAOAccompagniment extends DAO
{
    /**
     * Retourne toutes les infos
     *
     * @return array
     */
    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT * FROM Accompagniment');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Accompagniment');
        $res = $result->fetchAll();
        return $res;
    }


    

    /**
     * retrouve par l'id les informations
     *
     * @param  mixed $id
     *
     * @return array
     */
    public function retrieve($id)
    {
        $query = "SELECT * FROM Accompagniment WHERE id=" . $id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Accompagniment');
        return $result->fetch();
    }

    public function create($array)
    {
  
    }


    public function update($array)
    {

    }

    public function delete($id)
    {
     
    }
    public function getAllBy($filter)
    { }
}
