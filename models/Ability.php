<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Trainee;
use BWB\Framework\mvc\models\Skill;
use BWB\Framework\mvc\dao\DAOAbility;

class Ability extends DefaultModel
{
    //* Propriétés (cf bdd)
    protected $id;
    protected $Trainee_Account_id;
    protected $Skill_id;


    //* Constructeur

    public function __construct($id = null)
    {
        if(!is_null($id)){

            $this->parse((new DAOAbility())->retrieve($id));
        }
    }

   //  //* methode parse()
   //  public function parse($array)
   //  {
   //     $this->id = $array['id'];
   //     $this->Trainee_Account_id = $array['Trainee_Account_id'];
   //     $this->Skill_id = $array['Skill_id'];
   //  }

    //* Getters

    public function getId()
    {
       return $this->id;
    }

    public function getTrainnee_Account_id()
    {
       return $this->Trainee_Account_id;
    }

    public function getSkill_id()
    {
       return $this->Skill_id;
    }


    //* Setters 

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setTrainee_Account_id($Trainee_Account_id)
    {
        $this->Trainee_Account_id = new Trainee($Trainee_Account_id);
    }

    public function setSkill_id($Skill_id)
    {
      $this->Skill_id = new Skill($Skill_id);
    }
}