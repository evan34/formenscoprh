<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\dao\DAORole;
use BWB\Framework\mvc\models\DefaultModel;

class Role extends DefaultModel
{
    protected $id;
    protected $designiation;
    protected $description;

    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->parse((new DAORole())->retrieve($id));
        }
    }


    /** GETTERS */

    function getId()
    {
        return $this->id;
    }

    function getDesigniation()
    {
        return $this->designiation;
    }

    function getDescription()
    {
        return $this->description;
    }


    /** SETTERS */


    function setDesigniation($designiation)
    {
        $this->designiation = $designiation;
    }

    function setDescription($description)
    {
        $this->description = $description;
    }
}
