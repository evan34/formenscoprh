<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOTraining;

class Training extends DefaultModel
{
    protected $id;
    protected $name;
    protected $start;
    protected $end;
    protected $volume;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOTraining())->retrieve($id));
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setStart($start)
    {
        $this->end = $start;
    }

    public function getStart()
    {
        return $this->start;
    }
    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    public function getVolume()
    {
        return $this->volume;
    }
}