<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOSalaried;
use BWB\Framework\mvc\models\Account;

class Salaried extends DefaultModel
{
    protected $Account_id;

    public function __construct($Account_id = null)
    {
        if(!is_null($Account_id)){
        $this->parse((new DAOSalaried())->retrieve($Account_id));
        }
    }

    public function setAccount_id($Account_id)
    {
        $this->Account_id = new Account($Account_id);
    }

    public function getAccount_id()
    {
        return $this->Account_id;
    }
}